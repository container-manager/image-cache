FROM nginx

VOLUME /cache

COPY nginx.conf /etc/nginx/nginx.conf
